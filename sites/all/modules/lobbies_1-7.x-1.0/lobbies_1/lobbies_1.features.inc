<?php
/**
 * @file
 * lobbies_1.features.inc
 */

/**
 * Implements hook_node_info().
 */
function lobbies_1_node_info() {
  $items = array(
    'lobbies_1' => array(
      'name' => t('Lobbies 1'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
