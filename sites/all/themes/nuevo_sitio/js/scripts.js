jQuery(document).ready(function() {
	jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").attr("data-aos", "fade-right");
	jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").attr("data-aos-duration", "1500");

});

jQuery(window).load(function(){
	jQuery(".slider_general a.flex-next").click(function() {

		jQuery(".slider_general .views-field-title").removeClass("fadein-left");
		jQuery(".slider_general .views-field-title").hide();
		jQuery(".slider_general .views-field-view-node").removeClass("fadein-right");
		jQuery(".slider_general .views-field-view-node").hide();
		setTimeout(function() {
			jQuery(".slider_general .views-field-title").show();
			jQuery(".slider_general .views-field-view-node").show();
		},10);
		
		setTimeout(function() {
			jQuery(".slider_general .views-field-title").addClass("fadein-left");
			jQuery(".slider_general .views-field-view-node").addClass("fadein-right");
			
		},500);
	});
		
	jQuery(".slider_general a.flex-prev").click(function() {

		jQuery(".slider_general .views-field-title").removeClass("fadein-left");
		jQuery(".slider_general .views-field-title").hide();
		jQuery(".slider_general .views-field-view-node").removeClass("fadein-right");
		jQuery(".slider_general .views-field-view-node").hide();
		setTimeout(function() {
			jQuery(".slider_general .views-field-title").show();
			jQuery(".slider_general .views-field-view-node").show();
		},10);
		
		setTimeout(function() {
			jQuery(".slider_general .views-field-title").addClass("fadein-left");
			jQuery(".slider_general .views-field-view-node").addClass("fadein-right");
			
		},500);
	});
	
	jQuery(".slide_noticia_principal a.flex-next").click(function() {  
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").removeClass("aos-init");
		jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").removeClass("aos-init");
		jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").removeClass("aos-animate");
		jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").hide();
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").removeClass("aos-animate");
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").hide();
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").removeClass("aos-init");
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").removeClass("aos-animate");
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").hide();

		
		setTimeout(function() {
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").show();
			jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").show();
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").show();
		},10);
		
		setTimeout(function() {
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").addClass("aos-init");
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").addClass("aos-animate");
			jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").addClass("aos-init");
			jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").addClass("aos-animate");
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").addClass("aos-init");
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").addClass("aos-animate");
			
		},500);
	});
	
	jQuery(".slide_noticia_principal a.flex-prev").click(function() {  
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").removeClass("aos-init");
		jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").removeClass("aos-init");
		jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").removeClass("aos-animate");
		jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").hide();
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").removeClass("aos-animate");
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").hide();
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").removeClass("aos-init");
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").removeClass("aos-animate");
		jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").hide();

		
		setTimeout(function() {
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").show();
			jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").show();
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").show();
		},10);
		setTimeout(function() {
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").addClass("aos-init");
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n h2").addClass("aos-animate");
			jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").addClass("aos-init");
			jQuery(".slide_noticia_principal .views-field-field-imagen-slider-de-noticias").addClass("aos-animate");
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").addClass("aos-init");
			jQuery(".slide_noticia_principal .views-field-field-descripcion-slider-n p").addClass("aos-animate");
			
		},500);
    });	 
});

//Animaciones automáticas

jQuery(document).ready(function($) {
  $body = $('body');
  if($body.hasClass('front')){
    $('.flexslider').bind('before', function () {
        if('#flexslider-2'){
            $('.slider_general .views-field-title').removeClass('fadein-left');
            $('.slider_general .views-field-title').hide();
            $('.slider_general .views-field-view-node').removeClass('fadein-right');
            $('.slider_general .views-field-view-node').hide();
            
            setTimeout(function() {
                $('.slider_general .views-field-title').show();
                $('.slider_general .views-field-view-node').show();
            },10);
            
            setTimeout(function() {
                $('.slider_general .views-field-title').addClass('fadein-left');
                $('.slider_general .views-field-view-node').addClass('fadein-right');
            },500);
        }
        
        if('#flexslider-1'){
            $('.slide_noticia_principal .views-field-field-descripcion-slider-n h2').removeClass('aos-init');
            $('.slide_noticia_principal .views-field-field-imagen-slider-de-noticias').removeClass('aos-init');
            $('.slide_noticia_principal .views-field-field-imagen-slider-de-noticias').removeClass('aos-animate');
            $('.slide_noticia_principal .views-field-field-imagen-slider-de-noticias').hide();
            $('.slide_noticia_principal .views-field-field-descripcion-slider-n h2').removeClass('aos-animate');
            $('.slide_noticia_principal .views-field-field-descripcion-slider-n h2').hide();$('.slide_noticia_principal .views-field-field-descripcion-slider-n p').removeClass('aos-init');
            $('.slide_noticia_principal .views-field-field-descripcion-slider-n p').removeClass('aos-animate');$('.slide_noticia_principal .views-field-field-descripcion-slider-n p').hide();
            
            setTimeout(function() {
                $('.slide_noticia_principal .views-field-field-descripcion-slider-n p').show();
                $('.slide_noticia_principal .views-field-field-imagen-slider-de-noticias').show();$('.slide_noticia_principal .views-field-field-descripcion-slider-n h2').show();
            },10);
            
            setTimeout(function() {
                $('.slide_noticia_principal .views-field-field-descripcion-slider-n h2').addClass('aos-init');
                $('.slide_noticia_principal .views-field-field-descripcion-slider-n h2').addClass('aos-animate');
                $('.slide_noticia_principal .views-field-field-imagen-slider-de-noticias').addClass('aos-init');
                $('.slide_noticia_principal .views-field-field-imagen-slider-de-noticias').addClass('aos-animate');
                $('.slide_noticia_principal .views-field-field-descripcion-slider-n p').addClass('aos-init');
                $('.slide_noticia_principal .views-field-field-descripcion-slider-n p').addClass('aos-animate');
            },500);
        }
    });
  }
});