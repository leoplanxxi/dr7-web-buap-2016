name = interior 2
description = Plantilla Rector BUAP - Interior 2.
preview = preview.png
template = interior-layout

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/home/home.layout.css
stylesheets[all][] = css/layouts/home/home.layout.no-query.css
stylesheets[all][] = css/layouts/home/level-home.layout.css


; ========================================
; Scripts
; ========================================
scripts[] = js/web-contacto-rector.behaviors.js
scripts[] = js/web-contacto-rector.home.custom.js

; ========================================
; Regions
; ========================================
regions[branding] = Branding
regions[header] = Header
regions[navigation] = Navigation
regions[content] = Contenido
; regions[help] = Help
regions[footer] = Footer
