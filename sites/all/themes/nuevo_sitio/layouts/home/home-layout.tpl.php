<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['branding']: Items for the branding region.
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see omega_preprocess_page()
 */
global $base_url; 

?>
<meta name="google-site-verification" content="Inz11axcOQThPcAozhOA9GqYQ6BoUh_CxrT9ZlVxd2I" />
<!--<script>
(function ($) {
  Drupal.behaviors.onLoad = {
    attach: function (context, settings) {
      jQuery("#gsc-i-id1").attr("placeholder", "Buscar en BUAP");
    }
  };
})(jq214);


</script> //-->
<script src="<?php echo $base_url;?>/sites/all/themes/nuevo_sitio/libs/aos.js"></script>
<script src="<?php echo $base_url;?>/sites/all/themes/nuevo_sitio/js/scripts.js"></script>
<link href="<?php echo $base_url;?>/sites/all/themes/nuevo_sitio/libs/aos.css" rel="stylesheet">
<script src="<?php echo $base_url; ?>/sites/all/libraries/sidr/jquery.sidr.min.js"></script>
<script>
jQuery(window).load(function(){
  jQuery("#gsc-i-id1").attr("placeholder", "Buscar en BUAP");
  jQuery(".no-brincar > a").attr("href", "#");
});

AOS.init({
  duration: 1200,
});
</script>
<style>
@media only screen and (max-width: 1100px) {
	.l-redes-float {
		display: inline-block !important;
		position: fixed;
		z-index: 5000;
		left: 50%;
		transform: translateX(-50%);
		bottom: 0;
		background: #00395e;
		border-top-right-radius: 20px;
		padding: 5px;
		padding-bottom: 1px;
		border-top-left-radius: 20px;
		width: 220px;
	}

	.l-redes-float img {
		width: 32px;
	}
	
	.l-redes-float .redes_sociales a {
		padding-right: 10px;
	}
}

</style>
<div class="l-redes-float" style="display: none;">
	  <div class="redes_sociales">
		<a href="#" style="display: none;"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/ingles2.png" /></a>
		<a href="https://twitter.com/buapoficial" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/twitter.png" /></a>
		<a href="https://www.facebook.com/BUAPoficial" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/facebook.png" /></a>
		<a href="https://instagram.com/buapoficial/" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/instagram.png" /></a>
		<a href="https://www.youtube.com/user/ibuap" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/youtube.png" /></a>
		<a href="http://www.correo.buap.mx" target="_blank" style="padding-right: 0px"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/boton_correo_buap.png" /></a>
		</div>
	  </div>
<div class="l-page">
  <header class="l-header" role="banner">
	<div class="l-header-region">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
      <?php endif; ?>

      <?php if ($site_name || $site_slogan): ?>
        <?php if ($site_name): ?>
          <h1 class="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>
      <?php endif; ?>
	<?php global $base_url; ?>
	<div class="logo_buap" style="z-index: -1;">
		<a href="<?php echo $base_url .'/';  ?>">
		<img src="<?php echo $base_url . '/sites/all/themes/nuevo_sitio/css/img/escudo_blanco.png' ?>" />
		</a>
	</div>
	  <div class="l-menu">
      <?php print render($page['smart-menu']); ?>
	  </div>
	  <div class="l-redes">
	  <div class="redes_responsive">
		<a href="#" style="display: none;"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/ingles2.png" /></a>
		<a href="https://twitter.com/buapoficial" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/twitter.png" alt="Twitter" title="Twitter" /></a>
		<a href="https://www.facebook.com/BUAPoficial" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/facebook.png"  alt="Facebook" title="Facebook"/></a>
		<a href="https://instagram.com/buapoficial/" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/instagram.png"  alt="Instagram" title="Instagram"/></a>
		<a href="https://www.youtube.com/user/ibuap" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/youtube.png"  alt="YouTube" title="YouTube"/></a>
		<a href="http://www.correo.buap.mx" target="_blank"><img src="<?php echo $base_url; ?>/sites/all/themes/nuevo_sitio/img/boton_correo_buap.png"  alt="Correo BUAP" title="Correo BUAP"/></a>
		</div>
	  </div>
    </div>
</header>
 <div class="l-branding">
 <?php print render($page['highlighted']); ?>
   
	<?php print render($page['search']); ?>
	</div>
  <div class="l-main">
    <div class="l-content-a" role="main">

      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>

      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content-news-slider']); ?>
      <?php print $feed_icons; ?>

    </div>
	
	<div class="l-content-b">
		<div class="l-content-b-noticias" data-aos="fade-up" data-aos-duration="1000" style="overflow: hidden;">
			<?php print render($page['content-news']); ?>
		</div>
	</div>	  
	<div class="l-video">
		<div class="l-video-youtube" data-aos="zoom-in" data-aos-duration="800"  data-aos-anchor-placement="top-center" style="overflow: hidden; margin: 0 auto; display: block; max-width: 1300px;" >
		
			<?php print render($page['video']); ?>
		</div>
	  </div>
	<div class="parallax">
		<div class="parallax__layer parralax__layer--back l-content-c"></div>
		<div class="parallax__layer parralax__layer--base">
				<div class="l-content-c-events" data-aos="fade-up" data-aos-duration="800"  data-aos-anchor-placement="top-center" style="overflow: hidden;">
					<?php print render($page['content-events']); ?>
				</div><!--  data-aos-anchor-placement="bottom-bottom" -->
				<div class="l-content-c-convocations" data-aos="fade-left" data-aos-duration="800"  style="overflow: hidden;">
					<?php print render($page['content-convocations']); ?>
				</div>
			</div>
		</div>
	</div>
  

  <footer class="l-footer" role="contentinfo">
    <?php print render($page['footer']); ?>
  </footer>
</div>
