name = home
description = Plantilla Rector BUAP - Portada.
preview = preview.png
template = home-layout

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/home/home.layout.css
stylesheets[all][] = css/layouts/home/home.layout.no-query.css
stylesheets[all][] = css/layouts/home/level-home.layout.css


; ========================================
; Scripts
; ========================================
scripts[] = js/web-contacto-rector.behaviors.js
scripts[] = js/web-contacto-rector.home.custom.js

; ========================================
; Regions
; ========================================
regions[branding] = Branding
regions[header] = Header
regions[smart-menu] = Menu
regions[highlighted] = Highlighted
regions[navigation] = Navigation
regions[search] = Search
regions[video] = Video
regions[content-news-slider] = News Slider
regions[content-news] = News
regions[content-events] = Events
regions[content-convocations] = Convocations
regions[footer] = Footer
