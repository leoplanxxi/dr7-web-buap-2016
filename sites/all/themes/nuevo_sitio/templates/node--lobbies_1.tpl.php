<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

 global $base_url;
 $field_foto_2_l1 = field_get_items('node', $node, 'field_foto_2_l1');
 $field_sitio_web_l1 = field_get_items('node', $node, 'field_sitio_web_l1');

?>
<style>
        .col-a .enlace {
            width: 85%;
            padding: 10px 0px;
            background-color: #00C7F5;
            box-shadow: 1px 1px #53a7ea, 2px 2px #53a7ea, 3px 3px #53a7ea;
            color: white;
            text-align: center;
            margin: 0 auto;
        }

        .col-a .enlace a {
            color: white;
            width: 100%;
            display: block;
        }

    @media only screen and (min-width: 930px) {

        .l-content {
            width: 930px !important;
        }

        #node-<?php print $node->nid; ?> {
            width: 930px;
            display: block;
            margin: 0 auto;
        }

        .col-a {
            width: 310px;
            float: left;
            margin-right: 10px;
        }

        .col-b {
            width:600px;
            float: right;
            margin-left: 10px;
        }

        .col-a .foto-1 {
            padding-bottom: 20px;
            padding-top: 20px;
        }

        .col-a .foto-2, .col-a .datos {
            padding-bottom: 20px;
        }

        .col-a .datos p {
            margin-top: 0;
            text-align: right;
        }

    }

    @media only screen and (max-width: 930px) {
        .l-content {
            padding-top: 70px !important;   
        }
        .l-content .panel-pane.pane-page-content {
            padding-left: 35px !important;
            padding-right: 35px !important;
        }

        .col-a {
            width: 100%;
            float: right;
        }

        .col-a .foto-1 {
           text-align: center;
          
        }

        .col-a .foto-1 .field--name-field-foto-1-l1 {
            display: inline-block;
        }

        .col-a .foto-2 {
            display: none;
        }

        .col-b {
            width: 100%;
            float: left;
        }
		
			.col-a .enlace.escritorio {
			display: none;
		}
		
		.col-a .datos.escritorio {
			display: none;
		}
		
		.col-b .enlace.responsive {
			display: block; 
			margin-top: 20px;
		}
		
		.col-b .datos.responsive {
			display: block;
			margin-top: 20px;
		}
		
		.col-b .datos.responsive p {
			text-align: left !important;
		}
    }
</style>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content); ?>
      <div class="col-a">
        <div class="foto-1">
            <?php print render($content["field_foto_1_l1"]); ?>
        </div>
        <?php if ($field_foto_2_l1) : ?>
        <div class="foto-2">
            <?php print render($content["field_foto_2_l1"]); ?>
        </div>
        <?php endif; ?>
        <div class="datos escritorio">
            <?php print render($content["field_datos_l1"]); ?>
        </div>
        <?php if ($field_sitio_web_l1) : ?>
            <div class="enlace escritorio">
                <?php print render($content["field_sitio_web_l1"]); ?>
            </div>
        <?php endif; ?>
      </div>

      <div class="col-b">
        <div class="cuerpo">
            <?php print render($content["body"]); ?>
        </div>
		<div class="datos responsive">
            <?php print render($content["field_datos_l2"]); ?>
        </div>
		<?php if ($field_sitio_web_l2) : ?>
            <div class="enlace responsive">
                <?php print render($content["field_sitio_web_l2"]); ?>
            </div>
        <?php endif; ?>
      </div>

  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</div>
