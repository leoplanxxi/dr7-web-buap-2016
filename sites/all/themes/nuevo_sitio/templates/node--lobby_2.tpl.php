<?php
$items = field_get_items('node', $node, 'field_fotos_l2');
$mostrar_mapa = field_get_items("node", $node, "field_mostrar_mapa_l2");
$mapa = $mostrar_mapa[0]["value"];
?>
<style>
    .flexslider {
        margin-bottom: 15px !important;
    }

    .flex-control-nav {
        bottom: 5px !important;
    }
	
	.overlay-datos {
		position: absolute;
		bottom: 20px;
		left: 20px;
		background: rgba(0,0,0,0.75);
		color: white;
		font-family: sourcesans_extralight;
		font-size: 20px;
		padding: 20px;
	}

    .content .columna-datos {
        background: rgb(228, 227, 227);
        padding: 25px;
    }
	
	.flex-viewport ul li {
		position: relative;
	}

    .content .columna-link a {
        width: 100%;
        display: block;
        margin: 0 auto;
        text-align: center;
        background-color: #00C7F5;
        margin-top: 15px;
        box-shadow: 1px 1px #53a7ea, 2px 2px #53a7ea, 3px 3px #53a7ea;    
        padding: 10px 25px;
        color: white;
    }

    .content .columna-link a:hover {
        color: white;
        text-decoration: none;
    }

    .content .columna-datos-link {
        overflow: hidden;
    }

    .content .fila-info {
        margin-bottom: 20px;
		overflow: hidden;
    }

    .content .fila-mapa {
        width: 100%;
        float: left;
        border: 1px solid #00375f;
    }

    .content .fila-mapas #columnas {
        width: 100%;
    }

    .content .fila-mapa #columnas .contenido-columnas {
        position: relative;
        float: left;
        height: 500px;
        width: 35%;
        background: rgba(0, 55, 95, 0.64);
    }

    .content .fila-mapa #columnas .contenido-columnas ul {
        padding-right: 40px;
    } 

    .content .fila-mapa #columnas #cmapa {
        float: left;
        width: 65%;
        height: 500px;
    }

    .content .fila-mapa #columnas a {
        color: white;
        cursor: default;
    }

    .content .fila-mapa #columnas li {
        list-style: none; 
    }

    .content #map_canvas {
        width: 100%;
        height: 100%;
    }

    @media only screen and (min-width: 960px) {
        .content {
            width: 930px;
            display: block;
            margin: 0 auto;
        }

        .content .columna-introduccion {
            width: 550px;
            float: left;
            margin-right: 30px;
        }

        .content .columna-datos {
            width: 300px;
            float: left;
        }

        .content .columna-link {
            width: 300px;
            float: left;
        }

    }

    @media only screen and (max-width: 959px) {
        .content .fila-mapa {
            margin-top: 20px;
        }

        .content .columna-link a {
            width: inherit;
        }

        .content .fila-mapa ul {
            padding: 0;
        }

        .content .fila-mapa .contenido-columnas {
            padding: 10px;
        }
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider-min.js"></script>
<script>
    jQuery(window).load(function() {
        jQuery('.flexslider').flexslider({
            animation: "slide"
        });
    });
</script>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content); ?>
      <div class="slider-top">
        <div class="flexslider">
            <ul class="slides">
            <?php
                    foreach ($items as $item) {
                        echo "<li>";
                        $fc_value = field_collection_field_get_entity($item);
                        $datos["imagen"] = $fc_value->field_imagen_slider_l2["und"][0]["uri"];
                        $datos["link"] = $fc_value->field_enlace_slider_l2["und"][0];
                        
                        $style = "imagen_lobbies_2";

                        $derivative_uri = image_style_path($style, $datos["imagen"]);
                        $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $datos["imagen"], $derivative_uri);
                        $new_image_url  = file_create_url($derivative_uri); 
            
                        if ($datos["link"] == NULL) {
                            echo "<img src='" . $new_image_url . "'>";
                        }
                        else {
                            echo "<a href='" . $datos["link"]["url"] . "'><img src='" . $new_image_url . "'></a>";
                        }
						echo "<div class='overlay-datos'>";
						print render($content["field_datos_l2"]);
						echo "</div>";
                        echo "</li>";
                    }
                ?>

                </ul>
            </div>
        </div>
        <div class="fila fila-info">
            <div class="columna-introduccion">
                <?php print render($content["body"]); ?>
            </div>
            <div class="columna-datos-link">
               <?php/* <div class="columna-datos">
                    <?php print render($content["field_datos_l2"]); ?>
                </div> */?>
                <div class="columna-link">
                    <?php print render($content["field_sitio_web_l2"]); ?>
                </div>
            </div>
        </div>
        <div class="fila fila-extra">
            <?php print render($content["field_informaci_n_adicional_l2"]); ?>
        </div>
        <?php if ($mapa == "1") :?>
        <h3 align=center>Sedes</h3>
		<div class="fila fila-mapa">
		
        <?php 
                $itemsN = field_get_items('node', $node, 'field_mapa_w_l2');
                
        $valor_inicial = field_collection_field_get_entity($itemsN[0]);
        $latitud_inicial = $valor_inicial->field_mapa_mapa_w_l2["und"][0]["lat"];
        $longitud_inicial = $valor_inicial->field_mapa_mapa_w_l2["und"][0]["lng"];
         ?>
            <?php // Para el mapa dinámico  ?>
            <script async defer src="http://maps.google.cn/maps/api/js?key=AIzaSyADVQf-Il4q8fqdP0jutRSl3UuR8n7HJ50&v=3&sensor=false&callback=iniciar"  type="text/javascript"></script>
            <script type="text/javascript">
            function marcador(lati,lon) {
            var myLatlng = {lat: lati, lng: lon};
                var CU = new google.maps.LatLng(lati,lon); 
            var map = new google.maps.Map(document.getElementById("map_canvas"), {
                zoom: 17,
                center: CU,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                });
            var marker = new google.maps.Marker({
                position:myLatlng ,
                map: map,

            });
            } 
            </script>
            <script type="text/javascript">
            function iniciar() {
                //var CU = new google.maps.LatLng(19.003896, -98.202095); 
                var CU = new google.maps.LatLng(<?php echo $latitud_inicial; ?>, <?php echo $longitud_inicial; ?>); 
                var map = new google.maps.Map(document.getElementById("map_canvas"), {
                zoom: 17,
                center: CU,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var latlng = new google.maps.LatLng(<?php echo $latitud_inicial; ?>, <?php echo $longitud_inicial; ?>);
                var marker = new google.maps.Marker({
                    position: latlng
                });
                marker.setMap(map)
            }
            </script>
            <style>
            .zerowidth {
                width: 0px !important;
            }

            .nodisplay {
                display: none;
            }
            </style>
            <script>
                jQuery(document).ready(function() {
                    jQuery("#node-<?php print $node->nid; ?> #toggler-map").click(function(){
                        jQuery("#node-<?php print $node->nid; ?> #columnas").toggleClass("zerowidth");
                        jQuery("#node-<?php print $node->nid; ?> #columnas ul").toggleClass("nodisplay");
                    });
                    jQuery("#node-<?php print $node->nid; ?> .togglerMap").click(function(){
                        jQuery("#node-<?php print $node->nid; ?> #columnas").toggleClass("zerowidth");
                        jQuery("#node-<?php print $node->nid; ?> #columnas ul").toggleClass("nodisplay");
                    });
                });
                
            </script>
             <script>
           // google.maps.event.addDomListener(window, 'load', iniciar);
            </script>
    <?php
    ?>
    <div id="columnas"> 
        <div class="contenido-columnas">
        <ul>
            <?php 
                $i = 0;
                
                foreach ($itemsN as $item) 
                {
                    $fc_value = field_collection_field_get_entity($item);
                    $nombre = $fc_value->field_nombre_mapa_w_l2["und"][0]["value"];
                    $latit = $fc_value->field_mapa_mapa_w_l2["und"][0]["lat"];
                    $longi = $fc_value->field_mapa_mapa_w_l2["und"][0]["lng"];
                    echo '<div id="ctexto"><li><a onClick="javascript:marcador('.$latit.','.$longi.');"><p>'
                    . $nombre.'</a></p></li>';
                    //echo '<div id="ctexto"><li><a onClick="javascript:marcador('.$latit.','.$longi.');" class="togglerMap"><p>'
                   // . $nombre.'</a></p></li>';
                    $i++;
                }
            ?>
        </ul>
        </div>  
        <?php /*<div class="tab">
            <a href="javascript:void(0);" id="toggler-map">></a>
        </div> */ ?>
        <div id="cmapa"> 
           
            <div id="map_canvas"></div>
        </div> 

    </div>
    </div>
            <?php endif; ?>
  </div>


</div>
